% net lactate uptake rate, with regulated transporters (default case of the model) or oxygen independent transporters 
%
% model: 1: regulated transporters, 2: oxygen independent transporters
% param: struct containing the model parameters
%
% u: a function giving the lactate net uptake rate as a function of the oxygen concentration

function[u] = lactate_exchanges_functions(model, param)
    if model == 1 % default model
        u = @(lambda_i, l_e, o) param.k_u*l_e.*o./(param.L_u + o) - param.k_s*lambda_i*param.L_s./(param.L_s+ o);
    elseif model == 2 % oxygen independent oxygen exchanges
        u = @(lambda_i, l_e, o) param.k_u*l_e.*param.o_small./(param.L_u + param.o_small) - param.k_s*lambda_i*param.L_s/(param.L_s + param.o_large);
    end
end