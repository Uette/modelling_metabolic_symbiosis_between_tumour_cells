% one dimensional sensitivity analysis: effect of modifying one parameter (with logaritmic scale) on the ratio without over with transporters regulation
% param_name: name of the parameter we vary
% param: dimensionless parameter values
% original_param: original parameter values
% the selected parameters takes values 10^a with a from mini to maxi, a increasing with steps of step
% model: 1: model 1 (oxygen, lactate, cell density), 2: model 2 (oxygen, intracellular lactate, extracellular lactate, cell density)

function[] = sensitivity(param_name, param, original_param, step, mini, maxi, model)    
    default_param = param;
    values = default_param.(matlab.lang.makeValidName(param_name))*10.^(mini:step:maxi);
    nb = max(size(values));
    burden_ratio_values = NaN(2, nb);
    for j = 1:nb
        j
        param = default_param;
        param.(matlab.lang.makeValidName(param_name)) = values(j);
        if model == 1
            with = pde_1_adim(param, original_param, 0, NaN, NaN);
            param.D_l = 0;
            without = pde_1_adim(param, original_param, 0, NaN, NaN);
        elseif model == 2
            [with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, original_param, 1, 0);
            param.o_small = o_mini/original_param.o_min;
            param.o_large = o_maxi/original_param.o_min;
            without = pde_2_adim(param, original_param, 2, 0);
        end
        burden_ratio_values(1,j) = with;
        burden_ratio_values(2,j) = without;
    end
    plot(log10(values), burden_ratio_values(2,:)./burden_ratio_values(1,:), '.', 'MarkerSize', 14, 'Color', 'b')
    ylim([0, 1])
    xlim([log10(min(values)), log10(max(values))])
    ylabel('Tumour burden ratio')
    xlabel(['log_{10}(' param_name ')'])
    set(gca, 'FontSize', 32)
    set(gcf, 'Renderer', 'Painters')
    fig = gcf;
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = [0 0 20 20];
    print('-depsc', ['figures/model_' num2str(model) '_sensitivity_' param_name '.eps'])
end
