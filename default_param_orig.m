% function returning in a struct the values of the parameters in their
% original dimensions

function [param] = default_param_orig()

    c_star = 10^8; % typical cell density (estimated assuming cell diameter = 20 micro m)
    o_star = 0.08; % typical oxygen concentration (based on McGillen et al.)
    l_e_star = 1; % typical extracellular lactate concentration (based on McGillen et al.)
    lambda_i_star = l_e_star/c_star; % typical intracellular lactate concentration
    
    % diffusion
    param.D_o = 10^(-5); % oxygen diffusivity (based on McGillen et al.)
    param.D_l = 1.8*10^(-6); % lactate diffusivity (based on Phipps et al.)
    param.D_c = 6.6*10^(-11); % cell diffusivity (based on McGillen et al.)

    % metabolism
    param.k_o = 10^2*3/86400/o_star/c_star; % oxygen uptake saturation rate (based on McGillen et al.)
    param.c_star = c_star;
    param.s_l = 10^2*2/86400/c_star; % lactate export rate in the absence of oxygen (based on McGillen et al.)
    param.k_l = 10^2*1/86400/(l_e_star); % saturation lactate uptake rate (based on McGillen et al.)
    param.L_g = 0.05; % oxygen concentration of half saturated glycolysis (based on Giniunaite et al.)
    param.L_o = 0.01; % oxygen concentration of half saturated OXPHOS (based on Molavian et al.)
    
    % lactate export and uptake
    param.k_s = 10^2/86400/(lambda_i_star*c_star); % lactate export saturation rate (based on McGillen et al.)
    param.k_u = 10^2/86400/(l_e_star*c_star); % maximal lactate uptake rate (based on McGillen et al.)
    param.L_s = 0.05; % oxygen concentration of half saturated lactate export (chosen to be equal to L_g)
    param.L_u = 0.01; % oxygen concentration of half saturated uptake (chosen to be equal to L_o)

    % cell growth
    param.b_1 = 8.8*10^(-9); % cell birth rate parameter (based on de la Cruz et al.)
    param.b_2 = 0.2; % cell birth rate parameter (based on de la Cruz et al.)
    param.o_min = 0.007; % minimal oxygen concentration necessary for cell division (based on Ebbesen et al.)
    
    % cell death
    param.nu = 1/10/60/60/24; % death saturation rate (based on McGillen et al.)
    param.L_nu = 20000; % improving effect of the oxygen on the death rate (based on McGillen et al.)
    param.p = 2; % Hill coefficient in the death rate

    % boundary
    param.R = 0.1; % tumour radius
    param.J_o = 10^3; % flux of oxygen from the blood (based on McGillen et al.)
    param.J_l = 10; % flux of lactate from the blood (based on McGillen et al.)
    param.o_v = 0.0834; % concentration of oxygen in the blood (based on McGillen et al.)
    param.l_v = 0; % concentration of lactate in the blood (based on McGillen et al.)

end