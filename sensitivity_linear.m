% one dimensional sensitivity analysis: effect of modifying one parameter (with linear scale) on the ratio without over with transporters regulation
% param_name: name of the parameter we vary
% param: dimensionless parameter values
% original_param: original parameter values
% the selected parameters takes values 10^a with a from mini to maxi, a increasing with steps of step
% model: 1: model 1 (oxygen, lactate, cell density), 2: model 2 (oxygen, intracellular lactate, extracellular lactate, cell density)

function[] = sensitivity_linear(param_name, param, original_param, values, model)    
    param_default = param;
    nb = size(values, 2)
    burden_ratio_values = NaN(2, nb);
    for j = 1:nb
        j
        param = param_default;
        param.(matlab.lang.makeValidName(param_name)) = values(j);
        if model == 1
            with = pde_1_adim(param, 0);
            param.D_l = 0;
            without = pde_1_adim(param, 0);
        elseif model == 2
            [with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, 1, 0);
            param.o_small = o_mini/original_param.o_min;
            param.o_large = o_maxi/original_param.o_min;
            without = pde_2_adim(param, 2, 0);
        end
        burden_ratio_values(1,j) = with;
        burden_ratio_values(2,j) = without;
    end
    plot(values, burden_ratio_values(2,:)./burden_ratio_values(1,:), '.', 'MarkerSize', 14, 'Color', 'b')
    ylim([0, 1])
    ylabel('Tumour burden ratio')
    xlabel(param_name)
    set(gca, 'FontSize', 32)
    set(gcf, 'Renderer', 'Painters')
    fig = gcf;
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = [0 0 20 20];
    print('-depsc', ['figures/model_' num2str(model) '_sensitivity_' param_name '.eps'])
end
