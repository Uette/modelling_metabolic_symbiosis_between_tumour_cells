% model 2 (variables: oxygen, intracellular lactate, extracellular lactate, cell density)
% with radial symmetry and adimensionalized variables and parameters
%
% param: dimensionless parameter values
% original_param: parameters in their original dimension
% do_plot: 0: do not do figures, 1: plots in dimensionless variables, 2:
% plots in original variables
%
% tumor_burden: total tumor burden at the last time step
% o_max: maximal dimensionless oxygen value at the last time step
% lambda_i_max: maximal dimensionless intracellular lactate value at the last time step
% lambda_e_max: maximal dimensionless extracellular lactate value at the last time step
% c_max: maximal dimensionless cell density value at the last time step
% o_min: minimal dimensionless oxygen value at the last time step

function[tumor_burden, o_max, lambda_i_max, l_e_max, c_max, o_min] = pde_2_adim(param, original_param, model_exchanges, do_plot)    

    production = @(o) param.s_l*(1 - o./(o + param.L_g)); % lactate production by glycolysis
    consumption = @(o) param.k_l* o./(o + param.L_o); % lactate production consumption for OXPHOS
    growth = @(o, lambda_i) param.b_1*(o-1).^param.b_2.*(o>1) - lambda_i.^param.p./(o.^param.p+lambda_i.^param.p); % cell net growth rate
    u = lactate_exchanges_functions(model_exchanges, param); % lactate exchanges between cells and extracellular milieu
    
    % original variables
    t_0 = 1/original_param.nu;
    r_0 = sqrt(original_param.D_c/original_param.nu);
    o_0 = original_param.o_min;
    lambda_i_0 = original_param.L_nu*original_param.o_min/original_param.c_star;
    c_0 = original_param.c_star;
    l_e_0 = original_param.L_nu*original_param.o_min;
    
    % initial conditions
    o_init = param.o_v; % oxygen concentration
    lambda_i_init = 0; % intracellular lactate concentration
    l_e_init = param.l_v; % extracellular lactate concentration
    c_init = 1; % cell density
    
    % discretization parameters
    m = 100; % number of elements
    h = param.Y/m; % element size

    % ODE system matrices
    M_1 = spdiags(ones(m+1, 1)*[1 -2 1],-1:1, m+1, m+1);
    M_1(1, 2) = 2;
    M_1(m+1, m) = 2;
    
    M_2 = spdiags(ones(m+1, 1)*[-1 1],0:1, m+1, m+1);
    M_2(1, 1) = 0;
    M_2(1, 2) = 0;
    M_2(m+1, m+1) = 0;
    
    f_o = [zeros(m, 1); 2*param.D_o*param.J_o];
    f_l = [zeros(m, 1); 2*param.D_l*param.J_l];
    
    % integration
    T = 10^9/t_0; % final time
    rel_tol = 10^(-6); % relative tolerance threshold
    abs_tol = 10^(-6); % absolute tolerance threshold
    tspan = [0, T]; % time interval
    x_0 = [o_init*ones(m+1, 1); lambda_i_init*ones(m+1, 1); l_e_init*ones(m+1, 1); c_init*ones(m+1, 1)]; % initial condition
    options = odeset('RelTol', rel_tol, 'AbsTol', abs_tol);
    [t, x] = ode15s(@ode, tspan, x_0, options);
    
    % at the last time step
    o_last = x(end, 1:m+1);
    o_max = max(o_last*o_0);
    o_min = min(o_last*o_0);
    lambda_i_last = x(end, m+2:2*(m+1));
    lambda_i_max = max(lambda_i_last*lambda_i_0);
    l_e_last = x(end,2*(m+1)+1:3*(m+1));
    l_e_max = max(l_e_last*l_e_0);
    c_last = x(end, 3*(m+1)+1:end);
    c_max = max(c_last*c_0);
    
    % figures
    if do_plot == 1 % dimensionless variables
        % time evolution
        figure(1)
        plot(t, x(:,1:m+1))
        xlabel('Time \tau')
        ylabel('Oxygen concentration')

        figure(2)
        plot(t, x(:,m+2:2*(m+1)))
        xlabel('Time \tau')
        ylabel('Intracellular lactate per cell')
        
        figure(3)
        plot(t, x(:,2*(m+1)+1:3*(m+1)))
        xlabel('Time \tau')
        ylabel('Extracellular lactate concentration')

        figure(4)
        plot(t, x(:,3*(m+1)+1:end))
        xlabel('Time \tau')
        ylabel('Cell density')

        % solution at last time step
        figure(5)
        hold on
        plot(0:h:param.Y, o_last)
        xlabel('Radius y')
        ylabel('Oxygen concentration')

        figure(6)
        hold on
        plot(0:h:param.Y, lambda_i_last)
        xlabel('Radius y')
        ylabel('Intracellular lactate per cell')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2),1.1*max(lambda_i_last));
        set(gca, 'YLim', [0, limsy]);
        
        figure(7)
        hold on
        plot(0:h:param.Y, l_e_last)
        xlabel('Radius y')
        ylabel('Extracellular lactate concentration')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2),1.1*max(l_e_last));
        set(gca, 'YLim', [0, limsy]);

        figure(8)
        hold on
        plot(0:h:param.Y, c_last)
        xlabel('Radius y')
        ylabel('Cell density')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(c_last));
        set(gca, 'YLim', [0, limsy]);
        
        figure(9)
        hold on
        plot(0:h:param.Y, growth(o_last, lambda_i_last))
        xlabel('Radius y')
        ylabel('Growth rate')

    elseif do_plot == 2 % plot with original variables      
        figure(1)
        set(gcf, 'Renderer', 'Painters')
        plot(t*t_0, x(:,1:m+1)*o_0)
        xlabel('Time t (s)')
        ylabel('Oxygen concentration (mM)')

        figure(2)
        set(gcf, 'Renderer', 'Painters')
        plot(t*t_0, x(:,m+2:2*(m+1))*lambda_i_0)
        xlabel('Time t (s)')
        ylabel('Intracellular lactate per cell ({\mu}mol)')
        
        figure(3)
        set(gcf, 'Renderer', 'Painters')
        plot(t*t_0, x(:,2*(m+1)+1:3*(m+1))*l_e_0)
        xlabel('Time t (s)')
        ylabel('Extracellular lactate concentration (mM)')

        figure(4)
        set(gcf, 'Renderer', 'Painters')
        plot(t*t_0, x(:,3*(m+1)+1:end)*c_0)
        xlabel('Time t (t)')
        ylabel('Cell density (cm^{-3})')

        % solution at last time step
        figure(5)
        set(gcf, 'Renderer', 'Painters')
        hold on
        plot((0:h:param.Y)*r_0, o_last*o_0)
        xlabel('Radius r (cm)')
        ylabel('Oxygen concentration (mM)')

        figure(6)
        set(gcf, 'Renderer', 'Painters')
        hold on
        plot((0:h:param.Y)*r_0, lambda_i_last*lambda_i_0)
        xlabel('Radius r (cm)')
        ylabel('Intracellular lactate per cell ({\mu}mol)')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(lambda_i_last*lambda_i_0));
        set(gca, 'YLim', [0, limsy]);
        
        figure(7)
        set(gcf, 'Renderer', 'Painters')
        hold on
        plot((0:h:param.Y)*r_0, l_e_last*l_e_0)
        xlabel('Radius r (cm)')
        ylabel('Extracellular lactate concentration (mM)')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(l_e_last*l_e_0));
        set(gca, 'YLim', [0, limsy]);

        figure(8)
        set(gcf, 'Renderer', 'Painters')
        hold on
        plot((0:h:param.Y)*r_0, c_last*c_0)
        xlabel('Radius r (cm)')
        ylabel('Cell density (cm^{-3})')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(c_last*c_0));
        set(gca, 'YLim', [0, limsy]);
        
        % TODO
        figure(9)
        set(gcf, 'Renderer', 'Painters')
        hold on
        plot((0:h:param.Y)*r_0, growth(o_last, lambda_i_last))
        xlabel('Radius r (cm)')
        ylabel('Growth rate')
        
        figure(10)
        set(gcf, 'Renderer', 'Painters')
        hold on
        if model_exchanges == 1
            plot((0:h:param.Y)*r_0, o_last./(param.L_u + o_last))
        elseif model_exchanges == 2
            plot((0:h:param.Y)*r_0, ones(1, m+1)*param.o_small/(param.L_u + param.o_small))
        end
        xlabel('Radius r (cm)')
        ylabel('Dimensionless uptake rate')
        
        figure(11)
        set(gcf, 'Renderer', 'Painters')
        hold on
        if model_exchanges == 1
            plot((0:h:param.Y)*r_0, param.k_s * param.L_s./(param.L_s + o_last))
        elseif model_exchanges == 2
            plot((0:h:param.Y)*r_0, param.k_s*ones(1, m+1)*param.L_s/(param.L_s + param.o_large))
        end
        xlabel('Radius r (cm)')
        ylabel('Dimensionless export rate')
        %ylim([0 model_param.k_s])
        
        figure(12)
        set(gcf, 'Renderer', 'Painters')
        hold on
        plot((0:h:param.Y)*r_0, lambda_i_last.^param.p./(o_last.^param.p + lambda_i_last.^param.p))
        plot((0:h:param.Y)*r_0, lambda_i_last./(o_last + lambda_i_last))
        xlabel('Radius r (cm)')
        ylabel('Death rate')
    end
    
    % total tumor burden (original variables)
    %tumor_burden =  2*pi*(h*r_0)^3*((0:m-1).^2*(c_0*c_last(1:end-1)') + (1:m).^2*(c_0*c_last(2:end)')); % rectangular rule
    tumor_burden = 4*pi*r_0^3*c_0*h*(h^2*(1:m-1).^2*c_last(2:end-1)' + param.Y^2*c_last(end)/2); % trapezoidal rule

    
    function[x_prime] = ode(~,x)
            o = x(1:m+1); % oxygen concentration
            lambda_i = x(m+2:2*(m+1)); % intracellular lactate concentration per cell
            l_e = x(2*(m+1)+1:3*(m+1)); % extracellular lactate concentration
            c = x(3*(m+1)+1:end); % cell density
            
            x_prime = NaN(4*(m+1), 1);
            x_prime(1:m+1) = (param.D_o/h^2)*M_1*o + f_o.*(param.o_v-o)*(1/h + 1/param.Y) + (param.D_o*2/h^2)./([1 1:m]').*(M_2*o) - param.k_o*c.*o; % do/dt
            x_prime(m+2:2*(m+1)) = (1/h^2)./c.*((M_2*c).*(M_2*lambda_i)) + production(o) - consumption(o).*lambda_i + u(lambda_i, l_e, o) - lambda_i.*growth(o, lambda_i); % dl_i/dt
            x_prime(2*(m+1)+1:3*(m+1)) = (param.D_l/h^2)*M_1*l_e + f_l.*(param.l_v-l_e)*(1/h + 1/param.Y) + (param.D_l*2./h^2)./([1 1:m]').*(M_2*l_e) - u(lambda_i, l_e, o).*c; % dl_e/dt
            x_prime(3*(m+1)+1:end) = (1/h^2)*M_1*c + (2/h^2)./([1 1:m]').*(M_2*c) + growth(o, lambda_i).*c; % dc/dt
    end
end
