% function to adjust the spacing of items in a legend of a figure
% parameters: hobj: 'icons' object returned by legend creation function
%             incr: amount of spacing to add between the legend items

function adapt_legend_spacing( hobj, incr )
    incr_cur = incr;
    num_items = size(hobj, 1) / 3;
    for i = 2:num_items
        pos = get(hobj(i), 'Position')
        pos(2) = pos(2) - incr_cur;
        set(hobj(i), 'Position', pos);
    
        ydata = get(hobj((i-1)*2 + num_items + 1), 'YData')
        ydata(1) = ydata(1) - incr_cur;
        ydata(2) = ydata(2) - incr_cur;
        set(hobj((i-1)*2 + num_items + 1), 'YData', ydata);
    
        incr_cur = incr_cur + incr;
    end
end

