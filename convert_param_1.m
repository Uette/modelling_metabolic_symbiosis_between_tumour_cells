% compute dimensionless parameters for the model 1 (variables: oxygen, lactate, cell)
%
% param_original: parameters in their original dimension
%
% param: dimensionless parameters

function [param] = convert_param_1(param_original)
        
    param.D_o = param_original.D_o/param_original.D_c;
    param.k_o = (param_original.k_o*param_original.c_star)/param_original.nu;
    param.b_1 = param_original.b_1/param_original.nu;
    param.s_l = (param_original.s_l*param_original.c_star)/(param_original.nu*param_original.L_nu*param_original.o_min);
    param.L_g = param_original.L_g/param_original.o_min;
    param.L_o = param_original.L_o/param_original.o_min;
    param.k_l = param_original.k_l/param_original.nu;
    param.D_l = param_original.D_l/param_original.D_c;
    param.Y = sqrt(param_original.nu/param_original.D_c)*param_original.R;
    param.p = param_original.p;
    param.b_2 = param_original.b_2;
    param.J_o = sqrt(param_original.D_c/param_original.nu)*param_original.J_o;
    param.J_l = sqrt(param_original.D_c/param_original.nu)*param_original.J_l;
    param.o_v = param_original.o_v/param_original.o_min;
    param.l_v = param_original.l_v/param_original.o_min;

end