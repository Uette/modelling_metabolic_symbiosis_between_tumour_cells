
%%%% model 1 (variables: oxygen, lactate, cells)

% compare behavior with or without lactate diffusion 
original_param = default_param_orig(); % default original parameters
default_param = convert_param_1(original_param); % default dimensionless parameters
param = default_param;
names = {'figures/mod_1_time_o.eps', 'figures/mod_1_time_l.eps', 'figures/mod_1_time_c.eps', 'figures/mod_1_o.eps', 'figures/mod_1_l.eps', 'figures/mod_1_c.eps'};
plot_dimensions = {[0 0 21.1 20], [0 0 20 20.45], [0 0 20.2 20], [0 0 20 22.4], [0 0 20 20], [0 0 20 20]};
close all

with = pde_1_adim(param, original_param, 2, 'b', '-'); % tumour burden with lactate diffusion

for i = 1:3
    figure(i)
    set(gca, 'FontSize', 32)
    fig = gcf;
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = plot_dimensions{i}; % adjust size
    print('-depsc', names{i})
end

param.D_l = 0; % set lactate diffusivity to zero
without = pde_1_adim(param, original_param, 2, [0 0.5 0], '-'); % tumour burden without lactate diffusion

ratio = without/with % tumour burden ratio

figure(4) % legend
[leg, hobj] = legend('Lactate diffusion', 'No lactate diffusion');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',4);
ht = findobj(hobj,'type','text');
set(ht, 'FontSize', 32)
set(leg, 'Location', 'NorthOutside')
legend boxoff

for i = 4:6
    figure(i)
    set(gca, 'FontSize', 32)  % size axis labels
    fig = gcf;
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = plot_dimensions{i}; % adjust size
    print('-depsc', names{i}) % save figure as eps
end

% sensitivity to metabolic parameters
close all
step = 0.05;
mini = -1;
maxi = 1;
sensitivity('k_l', default_param, original_param, step, mini, maxi, 1) 
sensitivity('k_o', default_param, param_original_param, step, mini, maxi, 1) 
sensitivity('s_l', default_param, param_original_param, step, mini, maxi, 1) 


%%%% model 2 (variables: oxygen, intracellular lactate, extracellular lactate, cell density)
default_param = convert_param_2(original_param); % default original parameters
param = default_param; % default dimensionless parameters

% compare regulated lactate transporters, oxygen independent lactate
% transporters and no transporters at all
close all
[with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, original_param, 1, 2);
for i = 5:8
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', 'b')
end
%names = {'figures/mod_2_time_o_R.eps', 'figures/mod_2_time_li_R.eps', 'figures/mod_2_time_le_R.eps', 'figures/mod_2_time_c_R.eps', 'figures/mod_2_o_R.eps', 'figures/mod_2_li_R.eps', 'figures/mod_2_le_R.eps', 'figures/mod_2_c_R.eps'};
names = {'figures/mod_2_time_o.eps', 'figures/mod_2_time_li.eps', 'figures/mod_2_time_le.eps', 'figures/mod_2_time_c.eps', 'figures/mod_2_o.eps', 'figures/mod_2_li.eps', 'figures/mod_2_le.eps', 'figures/mod_2_c.eps'};
figure(1)
set(gca, 'FontSize', 32)
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 21.3 20];
%print('-depsc', names{1})
figure(2)
set(gca, 'FontSize', 32)
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20.1 20];
%print('-depsc', names{2})
figure(4)
set(gca, 'FontSize', 32)
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20.4 20];
print('-depsc', names{4})
for i = 3
    figure(i)
    set(gca, 'FontSize', 32)
    fig = gcf;
    fig.PaperUnits = 'centimeters';
    fig.PaperPosition = [0 0 20 20.5];
    %print('-depsc', names{i})
end
param.o_small = o_mini/original_param.o_min;
param.o_large = o_maxi/original_param.o_min;
without = pde_2_adim(param, original_param, 2, 2)
for i = 5:8
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', [0 0.5 0])  
end
without/with
param.k_u = 0;
param.k_s = 0;
pde_2_adim(param, original_param, 1, 2)
figure(6)
%xlim([0, 0.05])
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color', 'r')  
set(gca, 'FontSize', 32)
[leg, hobj] = legend('Regulated transporters', 'Constitutive transporters', 'No transporters');
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',4);
ht = findobj(hobj,'type','text');
set(ht, 'FontSize', 32)
set(leg, 'location', 'Bestoutside')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 33.65 20];
legend boxoff
%print('-depsc', names{6})
figure(5)
%xlim([0, 0.05])
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color', 'r')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 21.5 20];
%print('-depsc', names{5})
figure(8)
%xlim([0, 0.05])
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color', 'r')
fig = gcf;
fig.PaperUnits = 'centimeters';
%fig.PaperPosition = [0 0 20.8 20];
fig.PaperPosition = [0 0 20 20.2];
%print('-depsc', names{8})
figure(7)
%xlim([0, 0.05])
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color', 'r')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20 20];
%print('-depsc', names{7})

% sensitivity to L_s, particular values
param_default = convert_param_2(original_param); % default parameters
param = param_default;
close all
[with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, original_param, 1, 2);
for i = 5:11
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', 'b')
end
param.o_small = o_mini/param_original.o_min;
param.o_large = o_maxi/param_original.o_min;
without = pde_2_adim(param, original_param, 2, 2)
for i = 5:11
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', [0 0.5 0])  
end
without/with
param.L_s = 10*param_default.L_s;
[with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, original_param, 1, 2);
for i = 5:11
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', 'b', 'LineStyle',':')  
end
param.o_small = o_mini/param_original.o_min;
param.o_large = o_maxi/param_original.o_min;
without = pde_2_adim(param, original_param, 2, 2)
without/with
figure(7)
%xlim([0, 0.05])
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color', [0 0.5 0], 'LineStyle',':')  
set(gca, 'FontSize', 32)
[leg, hobj] = legend(['Regulated transporters' newline 'L_s = 7.14 (default)'], ['Constitutive transporters,' newline 'L_s = 7.14 (default)'], ['Regulated transporters' newline 'L_s = 35.7'], ['Constitutive transporters,' newline 'L_s = 35.7']);
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',4);
ht = findobj(hobj,'type','text');
set(ht, 'FontSize', 32)
set(leg, 'location', 'Bestoutside')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 33 20];
legend boxoff
adapt_legend_spacing(hobj, 0.06);
names = {'', '', '', '', '', 'figures/mod_2_L_s_lambda_i.eps', 'figures/mod_2_L_s_l_e.eps', 'figures/mod_2_L_s_c.eps', '', '', 'figures/mod_2_L_s_export.eps'};
print('-depsc', names{7})
figure(11)
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color',  [0 0.5 0],  'LineStyle',':')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 22 20];
ylim([0, 1000])
print('-depsc', names{11})
figure(8)
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color',  [0 0.5 0],  'LineStyle',':')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20.5 20];
print('-depsc', names{8})
figure(8)
%xlim([0, 0.05])
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color',  [0 0.5 0],  'LineStyle',':')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20.8 20];
print('-depsc', names{8})
figure(6)
%xlim([0, 0.05])
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color',  [0 0.5 0],  'LineStyle',':')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20 20.4];
print('-depsc', names{6})


% sensitivity to k_s, particular values
param_default = convert_param_2(original_param); % default parameters
param = param_default;
close all
[with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, original_param, 1, 2);
for i = 4:8
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', 'b')
end
param.o_small = o_mini/param_original.o_min;
param.o_large = o_maxi/param_original.o_min;
without = pde_2_adim(param, original_param, 2, 2)
for i = 5:8
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', [0 0.5 0])  
end
without/with
param.k_s = 100;
[with, o_maxi, ~, ~, ~, o_mini] = pde_2_adim(param, original_param, 1, 2);
for i = 5:8
    figure(i)
    hline = findobj(gcf, 'type', 'line');
    set(hline(1), 'Color', 'b', 'LineStyle',':')  
end
param.o_small = o_mini/param_original.o_min;
param.o_large = o_maxi/param_original.o_min;
without = pde_2_adim(param, original_param, 2, 2)
without/with

figure(8)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color', [0 0.5 0], 'LineStyle',':')  
set(gca, 'FontSize', 32)

[leg, hobj] = legend({['Regulated transporters' newline 'k_s = 1000 (default)'], ['Constitutive transporters' newline 'k_s = 1000 (default)'], ['Regulated transporters' newline 'k_s = 100'], ['Constitutive transporters' newline 'k_s = 100']});
hl = findobj(hobj,'type','line');
set(hl,'LineWidth',4);
ht = findobj(hobj,'type','text');
set(ht, 'FontSize', 32)
set(leg, 'location', 'Bestoutside')
legend boxoff
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 34 20];
adapt_legend_spacing(hobj, 0.06);
print('-depsc', 'figures/mod_2_k_s_c')

figure(6)
set(gca, 'FontSize', 32)
hline = findobj(gcf, 'type', 'line');
set(hline(1), 'Color',  [0 0.5 0],  'LineStyle',':')
fig = gcf;
fig.PaperUnits = 'centimeters';
fig.PaperPosition = [0 0 20.4 20];
print('-depsc', 'figures/mod_2_k_s_l')



% sensitivity to parameters
close all
param_default = convert_param_2(original_param); % default parameters
param = param_default;
step = 0.05;
min = -1;
max = 1;
sensitivity('s_l', param, param_original, step, min, max, 2)
sensitivity('k_l', param, param_original, step, min, max, 2)
sensitivity('k_o', param, param_original, step, min, max, 2)

sensitivity('k_u', param, param_original, step, min, max, 2)
sensitivity('k_s', param, param_original, step, min, max, 2)

sensitivity('L_u', param, param_original, 0.05, min, max, 2)
sensitivity('L_s', param, param_original, 0.05, min, max, 2)

values = [2.5*10.^(-2.5:0.05:-1.5)];
sensitivity_linear('L_u', param, param_original, values, 2)
values = [10.^(-3:0.5:1) 10:10:200];
sensitivity_linear('L_s', param, param_original, values, 2)


sensitivity('b_1', param, param_original, step, min, max, 2)

values = 1:0.1:4;
sensitivity_linear('p', param, param_original, values, 2)





