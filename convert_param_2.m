% compute dimensionless parameters for the model 2 (variables: oxygen, intracellular lactate, extracellular lactate, cell density)
%
% param_original: parameters in their original dimension
%
% param: dimensionless parameters

function [param] = convert_param_2(param_orig)

    param.D_o = param_orig.D_o/param_orig.D_c;
    param.k_o = (param_orig.k_o*param_orig.c_star)/(param_orig.nu);
    param.b_1 = param_orig.b_1/param_orig.nu;
    param.s_l = param_orig.s_l*param_orig.c_star/(param_orig.nu*param_orig.L_nu*param_orig.o_min);
    param.L_g = param_orig.L_g/param_orig.o_min;
    param.k_l = param_orig.k_l/param_orig.nu;
    param.L_o = param_orig.L_o/param_orig.o_min;
    param.k_u = param_orig.k_u*param_orig.c_star/param_orig.nu;
    param.L_u = param_orig.L_u/param_orig.o_min;
    param.k_s = param_orig.k_s/param_orig.nu;
    param.L_s = param_orig.L_s/param_orig.o_min;
    param.D_l = param_orig.D_l/param_orig.D_c;
    param.Y = sqrt(param_orig.nu/param_orig.D_c)*param_orig.R;
    param.p = param_orig.p;
    param.b_2 = param_orig.b_2;
    param.J_o = sqrt(param_orig.D_c/param_orig.nu)*param_orig.J_o;
    param.J_l = sqrt(param_orig.D_c/param_orig.nu)*param_orig.J_l;
    param.o_v = param_orig.o_v/param_orig.o_min;
    param.l_v = param_orig.l_v/(param_orig.L_nu*param_orig.o_min);
            
end