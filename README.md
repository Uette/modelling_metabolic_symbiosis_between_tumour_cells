# Modelling the relation between energy production and evolution of the cell density in a solid tumour

This repository contains Matlab code used for the master's thesis 'Modelling metabolic symbiosis in tumour growth', submitted on June 25 2018 at the Universitat Polytècnica de Catalunya. The code was tested on Matlab R2016b.

## Motivation
Anti-angiogenic drugs are clinically used to treat solid tumours. They inhibit angiogenesis, i.e. the formation of new blood vessels from pre-existing ones, for example by blocking pro-angiogenic signaling pathways. The idea of such therapies is to disrupt the supply of oxygen, glucose and other nutrients to the cancer cells, with the aim to starve them to death. However, it was observed in some cases (e.g. kidney tumours) that, despite temporary benefits, the tumour eventually resumes its growth. There are opposing views concerning the resistance mechanism. One controversial hypothesis is that cells in adjacent hypoxic and normoxic milieux interact through a metabolic symbiosis. According to it, by a differential regulation of lactate importers and exporters, lactate, a by-product of anaerobic metabolism in hypoxic regions, fuels aerobic metabolism in normoxic regions. Thus, it does not accumulate in hypoxic regions where it would become lethal. We build two systems of partial differential equations, describing at different levels of precision the relation between metabolism and evolution of the cell density within a tumour with a hypoxic core surrounded by better oxygenated regions.

## Matlab functions
* *pde_1_adim*: implements the first PDE model of this project, which desbribes the dynamics of the oxygen, lactate and cell density, in its nondimensionalized form. 
* *pde_2_adim*: implements the second PDE model of this project, which desbribes the dynamics of the oxygen, the intracellular lactate, the extracellular lactate and cell density, in its nondimensionalized form. 
*  *default_param_orig*: returns a struct with the default parameter values in their original dimension.
*  *convert_param_1* computes the dimensionless  parameters for Model 1.
*  *convert_param_2* computes the dimensionless parameters for Model 2.
*  *lactate_exchanges_functions*: returns a function which gives the net lactate uptake rate, for oxygen regulated transporters or oxygen independent transporters.
*  *sensitivity*: performs a sensitivity analysis with respect to a given parameter, with a logaritmic scale.
*  *sensitivity_linear*: performs a sensitivity analysis with respect to a given parameter, with a linear scale.
*  *adapt_legend_spacing*: helper function to adjust the spacing between items in a legend of a figure.
* *main*: code used to perform the simulations detailed in the master's thesis report.

## Example usage
Effect of preventing the lactate diffusion on the tumour burden:
``` 
original_param = default_param_orig(); % default original parameters
param = convert_param_1(original_param); % default dimensionless parameters
tumour_burden_default = pde_1_adim(param, original_param, 2, 'b', '-');
param.D_l = 0; % set lactate diffusivity to zero
tumour_burden_no_diffusion = pde_1_adim(param, original_param, 2, 'g', '-'); % tumour burden without lactate diffusion
ratio = tumour_burden_no_diffusion/tumour_burden_default % tumour burden ratio
```

Sensitivity to the parameter k_l:
```
original_param = default_param_orig(); % default original parameters
param = convert_param_1(original_param); % default dimensionless parameters
step = 0.05;
mini = -1;
maxi = 1;
sensitivity('k_l', param, original_param, step, mini, maxi, 1) 
```
