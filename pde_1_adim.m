% model 1 (variables: oxygen, lactate, cell)
% with radial symmetry and adimensionalized variables and parameters
%
% param: dimensionless parameter values
% original param: parameters in their original dimension
% do_plot: 0: do not do figures, 1: plots in dimensionless variables, 2:
% plots in original variables
%
% tumor_burden: total tumor burden at the last time step
% o_max: maximal dimensionless oxygen value at the last time step
% l_max: maximal dimensionless lactate value at the last time step
% c_max: maximal dimensionless cell density value at the last time step
% o_min: minimal dimensionless oxygen value at the last time step

function[tumor_burden, o_max, l_max, c_max, o_min] = pde_1_adim(param, original_param, do_plot, col, linestyle)    

    production = @(o) param.s_l*(1 - o./(o + param.L_g)); % lactate production by glycolysis  
    consumption = @(o) param.k_l*o./(o + param.L_o); % % lactate consumption by OXPHOS
    growth = @(o, l) param.b_1*(o-1).^param.b_2.*(o>1) - l.^param.p./(o.^param.p+l.^param.p); % cell net growth rate
     
    % original variables
    t_0 = 1/original_param.nu;
    r_0 = sqrt(original_param.D_c/original_param.nu);
    o_0 = original_param.o_min;
    l_0 = original_param.L_nu*original_param.o_min;
    c_0 = original_param.c_star;
       
    % initial conditions
    o_init = param.o_v; % oxygen concentration
    l_init = param.l_v; % lactate concentration
    c_init = original_param.c_star/c_0; % cell density
    
    % discretization parameters
    m = 100; % numner of space intervals
    h = param.Y/m; % interval size
    
    % ODE system matrices
    M_1 = spdiags(ones(m+1, 1)*[1 -2 1],-1:1, m+1, m+1);
    M_1(1, 2) = 2;
    M_1(m+1, m) = 2;
    
    M_2 = spdiags(ones(m+1, 1)*[-1 1],0:1, m+1, m+1);
    M_2(1, 1) = 0;
    M_2(1, 2) = 0;
    M_2(m+1, m+1) = 0;
    
    f_o = [zeros(m, 1); 2*param.D_o*param.J_o];
    f_l = [zeros(m, 1); 2*param.D_l*param.J_l];
    
    % integration
    T = 10^9/t_0; % final time
    rel_tol = 10^(-6); % relative tolerance threshold
    abs_tol = 10^(-6); % absolute tolerance threshold
    tspan = [0, T]; % time interval
    x_0 = [o_init*ones(m+1, 1); l_init*ones(m+1, 1); c_init*ones(m+1, 1)]; % initial condition
    options = odeset('RelTol', rel_tol, 'AbsTol', abs_tol);
    [t, x] = ode15s(@ode, tspan, x_0, options);
      
    % at the last time step
    o_last = x(end, 1:m+1);
    o_max = max(o_last*o_0);
    o_min = min(o_last*o_0);
    l_last = x(end, m+2:2*(m+1));
    l_max = max(l_last*l_0);
    c_last = x(end, 2*(m+1)+1:end);
    c_max = max(c_last*c_0);
    
    % figures
    if do_plot == 1 % plot dimensionless variables
        % time evolution
        figure(1)
        plot(t, x(:,1:m+1))
        xlabel('Time \tau')
        ylabel('Oxygen concentration')

        figure(2)
        plot(t, x(:,m+2:2*(m+1)))
        xlabel('Time \tau')
        ylabel('Lactate concentration')

        figure(3)
        plot(t, x(:,2*(m+1)+1:end))
        xlabel('Time \tau')
        ylabel('Cell density')

        % solution at last time step
        figure(4)
        hold on
        plot(0:h:param.Y, o_last, 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius y')
        ylabel('Oxygen concentration')

        figure(5)
        hold on
        plot(0:h:param.Y, l_last, 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius y')
        ylabel('Lactate concentration')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2),1.1*max(l_last));
        set(gca, 'YLim', [0, limsy]);

        figure(6)
        hold on
        plot(0:h:param.Y, c_last, 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius y')
        ylabel('Cell density')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(c_last));
        set(gca, 'YLim', [0, limsy]);
        
        figure(7)
        hold on
        plot(0:h:param.Y, growth(o_last, l_last), 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius y')
        ylabel('Growth rate')

    elseif do_plot == 2 % plot with original variables
        % time evolution
        figure(1)
        plot(t*t_0, x(:,1:m+1)*o_0)
        xlabel('Time t (s)')
        ylabel('Oxygen concentration (mM)')
        xlim([1,max(t*t_0)])

        figure(2)
        plot(t*t_0, x(:,m+2:2*(m+1))*l_0)
        xlabel('Time t (s)')
        ylabel('Lactate concentration (mM)')
        xlim([1,max(t*t_0)])

        figure(3)
        plot(t*t_0, x(:,2*(m+1)+1:end)*c_0)
        xlabel('Time t (s)')
        ylabel('Cell density (cm^{-3})')
        xlim([1,max(t*t_0)])

        % solution at last time step
        figure(4)
        hold on
        plot((0:h:param.Y)*r_0, o_last*o_0, 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius r (cm)')
        ylabel('Oxygen concentration (mM)')

        figure(5)
        hold on
        plot((0:h:param.Y)*r_0, l_last*l_0, 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius r (cm)')
        ylabel('Lactate concentration (mM)')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(l_last*l_0));
        set(gca, 'YLim', [0, limsy]);

        figure(6)
        hold on
        plot((0:h:param.Y)*r_0, c_last*c_0, 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius r (cm)')
        ylabel('Cell density (cm^{-3})')
        limsy = get(gca, 'YLim');
        limsy = max(limsy(2), 1.1*max(c_last*c_0));
        set(gca, 'YLim', [0, limsy]);
        
        figure(7)
        hold on
        plot((0:h:param.Y)*r_0, original_param.b_1*(o_last.*o_0/original_param.o_min-1).^original_param.b_2.*(o_last.*o_0>original_param.o_min) - original_param.nu*(l_last.*l_0).^original_param.p./((original_param.L_nu*o_last.*o_0).^original_param.p + (l_last.*l_0).^original_param.p), 'Color', col, 'Linestyle', linestyle)
        xlabel('Radius r (cm)')
        ylabel('Growth rate')
    end
    
    % total tumor burden (original variables)
    %tumor_burden =  2*pi*(h*r_0)^3*((0:m-1).^2*(c_0*c_last(1:end-1)') + (1:m).^2*(c_0*c_last(2:end)')); % rectangular rule
    tumor_burden = 4*pi*r_0^3*c_0*h*(h^2*(1:m-1).^2*c_last(2:end-1)' + param.Y^2*c_last(end)/2); % trapezoidal rule

    
    function[x_prime] = ode(~,x)
            o = x(1:m+1); % oxygen concentration
            l = x(m+2:2*(m+1)); % extracellular lactate concentration
            c = x(2*(m+1)+1:end); % cell density
            
            x_prime = NaN(3*(m+1), 1);
            x_prime(1:m+1) = (param.D_o/h^2)*M_1*o + f_o.*(param.o_v-o)*(1/h + 1/param.Y) + (param.D_o*2/h^2)./([1 1:m]').*(M_2*o) - param.k_o*c.*o; % do/dt
            x_prime(m+2:2*(m+1)) = (param.D_l/h^2)*M_1*l + f_l.*(param.l_v-l)*(1/h + 1/param.Y) + (param.D_l*2/h^2)./([1 1:m]').*(M_2*l) + production(o).*c - consumption(o).*l.*c; % dl_i/dt
            x_prime(2*(m+1)+1:end) = (1/h^2)*M_1*c + (2/h^2)./([1 1:m]').*(M_2*c) + growth(o, l).*c; % dc/dt
    end
end
